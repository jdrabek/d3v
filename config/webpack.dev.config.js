var webpack = require('webpack');
var path = require('path');
var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    devtool: 'eval',

    entry: [
        'babel-polyfill',
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './src/index.jsx'
    ],

    output: {
        filename: 'app.js',
        publicPath: '/dist',
        path: path.resolve('dist')
    },

    resolve: {
        extensions: ['', '.js', '.jsx'],
        modulesDirectories: ['src', 'node_modules'],
    },

    module: {
        loaders: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',            
            query  :{
                presets:['react','es2015']
            }            
        }, {
            test: /\.scss$/,
            loaders: ["style", "css", "sass"]
        }, {
            test: /\.(png|jpg|jpeg|gif|svg)$/,
            loader: 'url-loader',
            query: {
                name: '[path][name].[ext]?[hash]',
                limit: 3000000,

            }
        }, {
            test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-woff"
        }, {
            test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-woff"
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/octet-stream"
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000"
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=image/svg+xml"
        },
        {
            test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-otf"
        }
        ]
    },

    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new WebpackNotifierPlugin({
            alwaysNotify: true
        }),
    ]
};