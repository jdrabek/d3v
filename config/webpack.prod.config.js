var webpack = require('webpack');
var path = require('path');
 
module.exports = {
    devtool: 'eval',

    entry: [
        'babel-polyfill',
        './src/index.jsx'
    ],

    output: {
        filename: 'app.js',
        publicPath: '/dist',
        path: path.resolve('dist')
    },

    resolve: {
        extensions: ['.js', '.jsx'],
        modules: ['src', 'node_modules'],
    },

    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',            
            query  :{
                presets:['react','es2015']
            }            
        }, {
            test: /\.scss$/,
            loaders: ["style-loader", "css-loader", "sass-loader"]
        }, {
            test: /\.(png|jpg|jpeg|gif|svg)$/,
            loader: 'url-loader',
            query: {
                name: '[path][name].[ext]?[hash]',
                limit: 3000000,

            }
        }, {
            test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-woff"
        }, {
            test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-woff"
        }, {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/octet-stream"
        }, {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000"
        }, {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=image/svg+xml"
        },
        {
            test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
            loader: "url?limit=30000000&mimetype=application/font-otf"
        }
        ]
    },
 
};
