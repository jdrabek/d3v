import { AppContainer } from 'react-hot-loader';
import React from 'react';

import ReactDOM from 'react-dom';
import Root from './containers/Root';
import './styles/main.scss';

const rootElement = document.getElementById("app");

ReactDOM.render(
    <AppContainer>
        <Root />
    </AppContainer>,
    rootElement
);

if (module.hot) {
    module.hot.accept('./containers/Root', () => {
        const NextRoot = require('./containers/Root').default;

        ReactDOM.render(
            <AppContainer>
                <NextRoot />
            </AppContainer>,
            rootElement
        );
    });
}

