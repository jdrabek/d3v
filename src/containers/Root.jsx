import React from 'react';
import Header from 'containers/Header';
import Skills from 'containers/Skills';
import Footer from 'containers/Footer';
import { I18nextProvider } from 'react-i18next';

import i18n from './i18n'; // initialized i18next instance



export default class Root extends React.Component {

    render() {
        return (
            <I18nextProvider i18n={i18n}>
                <div className="row vertical-center">
                    <div id="box" className="animated fadeIn col-md-8 col-md-offset-2">
                        <Header />
                        <Skills />
                        <Footer />
                    </div>
                </div>
            </I18nextProvider>
        )
    }
}
