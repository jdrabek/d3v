import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6

export default class SkillRow extends React.Component {
    invertColor(hexTripletColor) {
        var color = hexTripletColor;
        color = color.substring(1);           // remove #
        color = parseInt(color, 16);          // convert to integer
        color = 0xFFFFFF ^ color;             // invert three bytes
        color = color.toString(16);           // convert to hex
        color = ("000000" + color).slice(-6); // pad with leading zeros
        color = "#" + color;                  // prepend #
        return color;
    }
    getColor(i) {
        let colors = [
            { bgColor: "#134F7E", fontColor: '#ffffff' },
            { bgColor: "#C6D4FF", fontColor: '#000000' },
        ];
        if (i === 0) {
            return colors[0];
        }
        return colors[1];
    }
    render() {
        const items = this.props.items.map((item, i) => {
            let classname = "col-md-2"
            // if (i === 0) {
            //     classname = classname + " col-md-offset-1"
            // }
            let color = this.getColor(i);
            return (
                <li key={i} style={{ backgroundColor: color.bgColor, color: color.fontColor, animation: 'flyInFromBottom 0.5s ease ' + (i + 1) * 0.1 + 's', opacity: '0', animationFillMode: 'forwards' }} className={classname} >
                    {item}
                </li>
            )
        });
        return (

            <div className="row" >
                <div className="col-md-12" style={{ margin: 0 }}>
                    <div className="row">
                        <ul className="skillslist">

                            {items}

                        </ul>
                    </div>
                </div >
            </div >

        );
    }
}