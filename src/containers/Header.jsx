import React from 'react';
import Flag from 'react-flags';
import { translate, Interpolate } from 'react-i18next';
import i18n from './i18n';

class Header extends React.Component {
    render() {
        const { t } = this.props;
    const toggle = lng => i18n.changeLanguage(lng);

        return (
            <div>
                <div className="row">
                    <div className="col-md-2 pull-right" style={{ margin: "-20px -45px 0 0" }}>
                       <a onClick={() => toggle('pl')}> <Flag  name="PL" format="png" pngSize={24} shiny={true} alt="PL Flag" basePath="img/flags/" style={{ marginLeft: "5px" }} /></a>
                       <a onClick={() => toggle('en')}> <Flag  name="GB" format="png" pngSize={24} shiny={true} alt="GB Flag" basePath="img/flags/"/></a>
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-3">
                        <img id="logo" src={require('images/d3v-logo.png')} className="" alt="d3v.eu logo" />
                    </div>
                    <div className="col-md-9">
                        <h2>
                            {t('greeting')}
                        </h2>
                        <p>{t('me')}</p>
                        <p>{t('offer')}</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default translate(['common'], { withRef: true })(Header);
