import React from 'react';
import CompanyDetails from 'containers/CompanyDetails';
import { translate, Interpolate } from 'react-i18next';

class Header extends React.Component {
    render() {
        const { t } = this.props;

        return (
            <div className="row">
                <div id="contact" className="col-md-12">
                    <h3>{t("getInTouch")}</h3>
                    <div className="row">
                        <div className="footerElement col-md-3"><i className="fa fa-envelope" aria-hidden="true"></i> jd@d3v.eu</div>
                        <div className="footerElement col-md-3"><i className="fa fa-phone" aria-hidden="true"></i> +48 667 336 693</div>
                        <div className="footerElement col-md-3"><i className="fa fa-linkedin" aria-hidden="true"></i><a href="http://linkedin.com/in/jakub-drabek-6243084b">jakub-drabek</a></div>
                        <div className="footerElement col-md-3"><CompanyDetails /></div>
                    </div>
                </div>
            </div>
        );
    }
}
export default translate(['common'], { withRef: true })(Header);