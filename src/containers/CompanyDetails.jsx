import React from 'react';
import { translate, Interpolate } from 'react-i18next';
class CompanyDetails extends React.Component {
    constructor() {
        super();
        this.state = {
            detailsVisible: false
        }
    }
    showDetails() {
        this.setState({
            detailsVisible: true
        })
    }

    render() {
        const { t } = this.props;

        const link = (
            <a id="companyDetails" onClick={this.showDetails.bind(this)}>
                <i className="fa fa-building-o" aria-hidden="true"></i>{t("companyDetails")}
            </a>
        )

        const details = (
            <div style={{ animation: 'flyInFromBottom 1.5s ease ' + 1 * 0.1 + 's', opacity: '0', animationFillMode: 'forwards' }}>
                Jakub Drabek<br />Software D3velopment<br />
                Wyzwolenia 87A<br />42-480 Poręba<br />
                NIP: 649-230-75-74  REGON: 366292370<br />
            </div>
        )
        return this.state.detailsVisible ? details : link
    }
}
export default translate(['common'], { withRef: true })(CompanyDetails);
