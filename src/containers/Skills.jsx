import React from 'react';
import SkillRow from 'containers/SkillRow';
import { translate, Interpolate } from 'react-i18next';


class Skills extends React.Component {


    render() {
        const { t } = this.props;

        return (
            <div className="row">
                <div id="skills" className="col-md-12">
                    <SkillRow items={[t("languages"), "PHP 5/7", "HTML5", "JavaScript", "CSS", "SQL"]} />
                    <SkillRow items={[t("tools"), "Grunt", "Composer", "Docker", "Vagrant", "Git"]} />
                    <SkillRow items={[t("databases"), "MySQL", "TransactSQL", "MongoDB", "SQLite", "Oracle"]} />
                    <SkillRow items={[t("testing"), "TDD", "BDD", "PHPUnit", "Codeception", "Selenium"]} />
                </div >
            </div >
        );
    }
}

export default translate(['common'], { withRef: true })(Skills);
